import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode
from datetime import date

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    #print(db, username, password, hostname, None)
    #db = 'ebdb'
    #username = 'testuser'
    #password = 'testpassword'
    #hostname = 'aailn7ouaphp5q.cqpzvcns8ttb.us-east-2.rds.amazonaws.com'

    #db = 'moviedb'
    #username = 'root'
    #hostname = '35.188.182.128'

    #port = 3306
    return db, username, password, hostname#, port

def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(ID INT UNSIGNED NOT NULL AUTO_INCREMENT, Year INT UNSIGNED NOT NULL, Title varchar(256) NOT NULL, Director varchar(256) NOT NULL, Actor varchar(256) NOT NULL, Release_Date date NOT NULL, Rating DOUBLE NOT NULL, PRIMARY KEY (ID))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    rd = date.today()

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    cur.execute("INSERT INTO movies (Year, Title, Director, Actor, Release_Date, Rating) values (2018, 'Test1', 'Darrin Hui', 'Darrin Hui', '%s', 5)" % rd)
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()
    entries = []
    data = {}

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    #search_by_title("Test2", cur)
    cur.execute("SELECT * FROM movies")
    #print(cur.fetchall())
    #entries = [dict(movie=row[0]) for row in cur.fetchall()]
    for row in cur.fetchall():
        for col in row:
            data = {'ID': row[0],
                 'Year': row[1],
                 'Title': row[2],
                 'Director': row[3],
                 'Actor': row[4],
                 'Release Date': row[5],
                 'Rating': row[6]
                }
        entries.append(data)
    #print(entries[0]['Title'])
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

#Searches database for specific title and returns the ID if found
def search_by_title(title, cur):
    i = -1
    print("Searching for movie: %s" % title)
    cur.execute("SELECT * FROM movies")
    for row in cur.fetchall():
        if row[2].lower() == title.lower():
            i = row[0]
            print("Found movie: %s" % title)
            break
    return i

#Helper method to parse release_date month into a number for date object
def get_month(month):
    return {
        'Jan':1,
        'Feb':2,
        'Mar':3,
        'Apr':4,
        'May':5,
        'Jun':6,
        'Jul':7,
        'Aug':8,
        'Sep':9,
        'Oct':10,
        'Nov':11,
        'Dec':12
    }.get(month, 'ERROR')

#Helper method to get python date format
def get_date(release_date):
    date_list = release_date.split()
    if len(date_list) != 3:
        return render_template('index.html', message = "Date format is incorrect. It should be: DD/MON/YYYY")
    day = int(date_list[0])
    month = get_month(date_list[1])
    if month == 'ERROR':
        print('Month format is incorrect')
        return render_template('index.html', message = "Month format is incorrect")
    date_year = int(date_list[2])
    rd = date(date_year, month, day)
    return rd

@app.route('/add_movie', methods=['POST'])
#def add_movie(year, title, director, actor, release_date, rating):
def add_movie():
    msg = ''
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    rating = float(rating)
    print(type(rating))
    print(rating)
    print("Adding movie: %s" % title)
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        rd = get_date(release_date)
        exists = search_by_title(title, cur)
        if exists == -1:
            cur.execute("INSERT INTO movies (Year, Title, Director, Actor, Release_Date, Rating) VALUES('%s', '%s', '%s', '%s', '%s', '%s') ON DUPLICATE KEY UPDATE Year='%s', Title='%s', Director='%s', Actor='%s', Release_Date='%s', Rating='%s'" % (year, title, director, actor, rd, rating, year, title, director, actor, rd, rating))
            msg = ("Movie %s successfully inserted" % title)
        else:
            cur.execute("UPDATE movies SET Year='%s', Title='%s', Director='%s', Actor='%s', Release_Date='%s', Rating='%s' WHERE ID='%s'" % (year, title, director, actor, rd, rating, exists))
            msg = ("Movie %s successfully updated" % title)
    except Exception as e:
        msg = ("Movie %s could not be inserted - %s" % (title, e))
        #return render_template('index.html', message = msg)
    cnx.commit()
    return render_template('index.html', message = msg)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    msg = ''
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    print("Updating movie: %s" % title)
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    rd = get_date(release_date)
    exists = search_by_title(title, cur)
    if exists == -1:
        msg = ("Movie with %s does not exist" % title)
    try:
        cur.execute("UPDATE movies SET Year='%s', Title='%s', Director='%s', Actor='%s', Release_Date='%s', Rating='%s' WHERE ID='%s'" % (year, title, director, actor, rd, rating, exists))
        msg = ("Movie %s successfully updated" % title)
    except Exception as e:
        msg = ("Movie %s could not be updated - %s" % (title, e))
        #return render_template('index.html', message = msg)
    cnx.commit()
    return render_template('index.html', message = msg)

@app.route('/delete_movie', methods = ['POST'])
def delete_movie():
    msg = ''
    title = request.form['delete_title']
    print("Deleting movie: %s" % title)
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    exists = search_by_title(title, cur)
    if exists == -1:
        msg = ("Movie with %s does not exist" % title)
        return render_template('index.html', message = msg)
    try:
        cur.execute("DELETE FROM movies WHERE ID='%s'" % exists)
        msg = ("Movie %s successfully deleted" % title)
    except Exception as e:
        msg = ("Movie %s could not be deleted - %s" % (title, e))
    cnx.commit()
    return render_template('index.html', message = msg)

#Helper method to retrieve list of movies by actor
def search_by_actor(actor, cur):
    movie_list = []
    print("Searching for actor: %s" % actor)
    cur.execute("SELECT Title, Year, Actor FROM movies")
    for row in cur.fetchall():
        if row[2].lower() == actor.lower():
            movie_list.append(row)
    return movie_list

@app.route('/search_movie', methods = ['GET'])
def search_movie():
    msg = ''
    movie_list = []
    actor = request.args.get('search_actor')
    print("Searching for movies with actor: %s" % actor)
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    movie_list = search_by_actor(actor, cur)
    if len(movie_list) < 1:
        msg = ("No movies found with actor %s" % actor)
        return render_template('index.html', message = msg)
    else:
        entries = []
        entry = []
        print(movie_list)
        for movie in movie_list:
            entry = ("%s, %s, %s" % (movie[0], movie[1], movie[2]))
            entries.append(entry)
        return render_template('index.html', entries = entries)

@app.route('/highest_rating', methods = ['GET'])
def highest_rating():
    msg = ''
    entries = []
    max_rating = 0
    print("Searching for movie(s) with highest rating")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT Title, Year, Actor, Director, Rating FROM movies ORDER BY Rating DESC")
        table = cur.fetchall()
        first = table[0]
        entries.append("%s, %s, %s, %s, %s" % (first[0], first[1], first[2], first[3], first[4]))
        max_rating = first[4]
        table.pop(0)
        if not table:
            return render_template('index.html', entries = entries)
        for row in table:
            if row[4] == max_rating:
                entries.append("%s, %s, %s, %s, %s" % (row[0], row[1], row[2], row[3], row[4]))
            else:
                break
    except Exception as e:
        msg = ("Error - %s" % e)
        return render_template('index.html', message = msg)

    return render_template('index.html', entries = entries)

@app.route('/lowest_rating', methods = ['GET'])
def lowest_rating():
    msg = ''
    entries = []
    min_rating = 0
    print("Searching for movie(s) with lowest rating")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT Title, Year, Actor, Director, Rating FROM movies ORDER BY Rating")
        table = cur.fetchall()
        first = table[0]
        entries.append("%s, %s, %s, %s, %s" % (first[0], first[1], first[2], first[3], first[4]))
        min_rating = first[4]
        print(type(first[4]))
        print(first[4])
        table.pop(0)
        if not table:
            return render_template('index.html', entries = entries)
        for row in table:
            if row[4] == min_rating:
                entries.append("%s, %s, %s, %s, %s" % (row[0], row[1], row[2], row[3], row[4]))
            else:
                break
    except Exception as e:
        msg = ("Error - %s" % e)
        return render_template('index.html', message = msg)

    return render_template('index.html', entries = entries)

#@app.route('/add_to_db', methods=['POST'])
#def add_to_db():
#    print("Received request.")
#    print(request.form['message'])
#    msg = request.form['message']

#    db, username, password, hostname = get_db_creds()
#
#   cnx = ''
#    try:
#        cnx = mysql.connector.connect(user=username, password=password,
#                                      host=hostname,
#                                      database=db)
#    except Exception as exp:
#        print(exp)
#        import MySQLdb
#        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

#    cur = cnx.cursor()
#    cur.execute("INSERT INTO message (greeting) values ('" + msg + "')")
#    cnx.commit()
#    return hello()


@app.route("/")
def hello():
#    print("Printing available environment variables")
#    print(os.environ)
#    print("Before displaying index.html")
#    entries = query_data()
#    print("Entries: %s" % entries)
#    return render_template('index.html', entries=entries)
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    create_table()
    #add_movie(1952, 'Test2', 'abc', 'def', '21 Jun 2594', 5)
    app.run(host='0.0.0.0')
