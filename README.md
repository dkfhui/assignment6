name: Darrin Hui
eid: dh34554
bitbucket id: dkfhui
project id: assignment6-202322
url: http://35.192.82.55
Jenkins url: http://13.58.174.65:8080
ec2 instance: ubuntu@ec2-13-58-174-65.us-east-2.compute.amazonaws.com

comments: I completely forgot my Jenkins login credentials but the orginal password is: 6466ab4658c443f5a9ebfe9a82106c36. 
I also couldn't add the public keys to the Jenkins server.  The static link to the website also doesn't seem to work for 
some reason although it did before I used the ingress files and added the ingress node. All the CI seemed to be working before
though; each commit triggered a build that I could view in Jenkins before I locked myself out. I'm really not sure what to do
about all this. I also realize that this commit is after the deadline even though the last time I committed was days before
the deadline.
